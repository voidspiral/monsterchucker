/* http://www.network-science.de/ascii/

# #    # # ##### 
# ##   # #   #   
# # #  # #   #   
# #  # # #   #   
# #   ## #   #   
# #    # #   # 

*/

let data = {}
data.MTheader = null;
data.trash = {};
data.isDirty = false;
let inputTypes = [
	'input[type="checkbox"]',
	'input[type="radio"]',
	'input[type="text"]',
	'textarea',
	'input[type="number"]',
	'select',
]
data.tables = {};
state = {};

/*

#     #               
##    #   ##   #    # 
# #   #  #  #  #    # 
#  #  # #    # #    # 
#   # # ###### #    # 
#    ## #    #  #  #  
#     # #    #   ##   
                      
*/
function SetupNav (){
	var navelement = document.getElementsByTagName("nav")[0];
	//console.log(navelement.children);
	for (let i = 0; i < navelement.children.length; i++){
		var button = navelement.children[i];
		button.addEventListener("change", SwitchPage);
	}
}
function SwitchPage(e){
	// show the right page
	let target = e.srcElement.getAttribute('value');
	SetPage(target);
}
function SetPage (target){
	// hide all pages
	let pages = document.querySelectorAll(".page");
	//console.log(pages);
	for (var i = pages.length - 1; i >= 0; i--) {
		pages[i].classList.remove("activePage");
	}
	// activate the page
	let page = gid(target);
	page.classList.add("activePage");
	state.page = target;
	// set the right radio button
	let targetRadio = document.querySelector('[name="page"][value="' + target + '"]');
	//console.log(targetRadio)
	if(!targetRadio.checked){
		targetRadio.checked = true;
	}
	SaveState();
}

/*

#     #                                          
##   ##  ####  #    #  ####  ##### ###### #####  
# # # # #    # ##   # #        #   #      #    # 
#  #  # #    # # #  #  ####    #   #####  #    # 
#     # #    # #  # #      #   #   #      #####  
#     # #    # #   ## #    #   #   #      #   #  
#     #  ####  #    #  ####    #   ###### #    # 
                                                 
#######                             
   #      ##   #####  #      ###### 
   #     #  #  #    # #      #      
   #    #    # #####  #      #####  
   #    ###### #    # #      #      
   #    #    # #    # #      #      
   #    #    # #####  ###### ###### 

*/

function MTInitTable () {
	//let monsters = data.monsters;
	let monstercount = Object.keys(data.monsters).length;
	for (let i = 0; i < monstercount; i++){
		let monsterName = data.monsters[Object.keys(data.monsters)[i]].monsterName;
		//console.log(monsterName);
		MTAddRow(monsterName);
	}
	UpdateEditList();
}
function MTAddRow (monsterName){
	let newrow = document.createElement('tr');
	newrow.classList.add("monster");
	newrow.setAttribute('data-monster', monsterName);
	let children = [].slice.call(data.MTheader.children);
	children.forEach(function(child){
		let newcell = document.createElement('td');
		newcell.innerHTML = '';
		newcell.setAttribute('data-field', child.getAttribute('data-field'));
		newcell.setAttribute('data-calc', child.getAttribute('data-calc'));
		newrow.append(newcell);
	});
	data.MTheader.parentElement.append(newrow);
	MTPopulateRow(monsterName);

}
function MTPopulateRow (monsterName) {
	// Get the target row
	let row = GetRowFromName(monsterName);
	let monsterObject = data.monsters[monsterName];
	// iterate through cells and get values from monster
	let cells = Array.from(row.getElementsByTagName('td'));
	cells.forEach( function(cell, i) {
		// which value should we get?
		let field = cell.getAttribute('data-field');
		//console.log(field);
		// for some insane magical reason cell.getAttributes() returns 'null' as a string, not null.
		if (field !== 'null') {
			cell.innerHTML = monsterObject[field];
		}
		else {
			let type = cell.getAttribute('data-calc');
			//console.log(type)
			//value = type;
			switch (type){
				case 'defenses':
					cell.innerHTML = 'd';
					break;
				case 'qualities':
					cell.innerHTML = 'q';
					break;
				case 'actions':
					cell.innerHTML = 'a';
					break;
				case 'buttons':
					cell.innerHTML = '';
					let editButton = document.createElement('button');
					editButton.innerHTML = 'E';
					cell.appendChild(editButton);
					let listButton = document.createElement('button');
					listButton.innerHTML = '+';
					cell.appendChild(listButton);
					let delButton = document.createElement('button');
					delButton.innerHTML = 'X';
					delButton.addEventListener('click', MTRowDeleteButton);
					cell.appendChild(delButton);
					break;
			}
		}
	});
}
function MTRowDeleteButton (e) {
	// get the monster name of the row
	let button = e.srcElement;
	let row = button.parentElement.parentElement;
	let monsterName = row.getAttribute('data-monster');
	console.log(monsterName);
	MTDeleteMonsterByName(monsterName);
}
function MTDeleteMonsterByName(monsterName){
	// Actually move them to data.trash
	let monsterObject = data.monsters[monsterName];
	data.trash[monsterName] = monsterObject;
	delete data.monsters[monsterName];
	console.log(data.trash);
	// Remove the row
	let row = GetRowFromName(monsterName);
	row.outerHTML = "";

}
function MTGetColumns () {
	data.MTheader = document.querySelector("#MonsterTable table tr");
	//console.log(data.MTheader);
}

/*

#    #  ####  #    #  ####  ##### ###### #####  
##  ## #    # ##   # #        #   #      #    # 
# ## # #    # # #  #  ####    #   #####  #    # 
#    # #    # #  # #      #   #   #      #####  
#    # #    # #   ## #    #   #   #      #   #  
#    #  ####  #    #  ####    #   ###### #    # 
                                                
                                    
###### #####  # #####  ####  #####  
#      #    # #   #   #    # #    # 
#####  #    # #   #   #    # #    # 
#      #    # #   #   #    # #####  
#      #    # #   #   #    # #   #  
###### #####  #   #    ####  #    # 

*/

function UpdateEditList () {
	// find the dropdown
	let select = gid('editingMonster');
	// remove children
	select.innerHTML = '<option>Select...</option>';
	// set up list
	//console.log(data.monsters);
	for (let i = 0; i < Object.keys(data.monsters).length; i++){
		monsterName = Object.keys(data.monsters)[i];
		//console.log(monsterName);
		let option = document.createElement('option');
		option.innerHTML = monsterName;
		select.appendChild(option);
	}
}
function SaveMonster () {
	let monsterObject = CreateMonsterObject();
	let monsterName = monsterObject.monsterName;
	console.log(monsterObject);
	data.monsters[monsterName] = monsterObject;
	// update the row if it exists, otherwise create new
	let row = GetRowFromName(monsterName);
	console.log(row);
	if (row){
		MTPopulateRow(monsterName);
	}
	else {
		MTAddRow(monsterName);
	}
	UpdateEditList();
	SetEditorClean();
}
function CreateMonsterObject(){
	let monsterObject = {};
	let editorDiv = gid('MonsterEditor');
	// Do each type of input, 1 at a time
	inputTypes.forEach(function(type){
		SerializeInputs(type, editorDiv, monsterObject)
	});
	//console.log(labels);
	console.log(JSON.stringify(monsterObject, null, 2));
	return monsterObject;
}
function SerializeInputs(queryString, editorDiv, monsterObject){
	let elements = editorDiv.querySelectorAll(queryString);
	for (let i = elements.length - 1; i >= 0; i--) {
		let element = elements[i];
		let value = element.value;
		let key = element.id;
		// The only input type that we use that doesn't use `.value`
		// is checkbox, so we catch that specifically
		if (queryString.includes('checkbox') || queryString.includes('radio')){
			value = element.checked;
		}
		if (element.hasAttribute('disabled') || !key || !value){
			continue;
		}
		monsterObject[key] = value;
	}
}
function SetupFields() {
	// get a list of all updatable fields
	let editorDiv = gid('MonsterEditor');
	let fields = [];
	inputTypes.forEach(function(type){
		//console.log(editorDiv.querySelectorAll(type));
		fields = fields.concat(Array.prototype.slice.call(editorDiv.querySelectorAll(type)));
	});
	//console.log(fields);
	fields.forEach(function(field){
		// add event listener to each
		if (!field.hasAttribute('disabled')){
			field.addEventListener('change', UpdateCalculatedFields);
		}
	});
}
function UpdateCalculatedFields(){
	// In this function, we recalculate ALL calculated fields, all at once
	MELookupBaseline();
	MEUpdateXpValue();
	MEUpdateStatMods();
	MEUpdateSaves();
	MEUpdateAttackBonus();
	MEUpdateSaveDc();
	MEUpdateAverageDamage();
	MEUpdateDefenses();
	MECalculateEHP();
	MECalculateEAC();
	MECalculateCR();
	//console.log(fields);
}
function MELookupBaseline() {
	let cr = gid('calcTargetCr').value || 0;
	// get fields
	let profBonusField = 	gid('calcBaseProfBonus');
	let acField = 			gid('calcBaseAc');
	let hpField = 			gid('calcBaseHp');
	let attackBonusField = 	gid('calcBaseAttackBonus');
	let dprField = 			gid('calcBaseDpr');
	let saveDcField = 		gid('calcBaseSaveDc');
	// get values
	let baseProfBonus = 	data.tables.crStats[cr]['profBonus'];
	let baseAc = 			data.tables.crStats[cr]['armorClass'];
	let baseHpMin = 		data.tables.crStats[cr]['hitPointMin'];
	let baseHpMax = 		data.tables.crStats[cr]['hitPointMax'];
	let baseAttackBonus = 	data.tables.crStats[cr]['attackBonus'];
	let baseDprMin =		data.tables.crStats[cr]['damagePerRoundMin'];
	let baseDprMax =		data.tables.crStats[cr]['damagePerRoundMax'];
	let baseSaveDc = 		data.tables.crStats[cr]['saveDc'];
	// make compound values
	let baseDpr = baseDprMin + '–' + baseDprMax;
	let baseHp = baseHpMin + '–' + baseHpMax;
	// put values to ui
	profBonusField.value = baseProfBonus;
	acField.value = baseAc;
	hpField.value = baseHp;
	attackBonusField.value = baseAttackBonus;
	dprField.value = baseDpr
	saveDcField.value = baseSaveDc;
	gid('calcNumHitDice').placeholder = cr;
}
function MEUpdateXpValue() {
	// get the CR
	let cr = gid('monsterCr').dataset.crDecimal || 0;
	// lookup the XP value on the chart
	let xp = data.tables.crStats[cr]['xp'];
	let xpField = gid('monsterXp');
	xpField.value = xp;
	//console.log(xp);
}
function MEUpdateStatMods(){
	data.tables.stats.forEach(function(stat){
		MEUpdateSingleStatMod(stat);
	});
}
function MEUpdateSingleStatMod (stat){
	// get stat score
	let score = parseInt(gid('stat' + stat).value);
	// calculate mod
	let mod = Math.floor(( score - 10 ) / 2);
	// get ui field
	let modField = gid( GetAcronym(stat) + 'Mod');
	// put value in ui
	modField.value = mod;
}
function MEUpdateSaves () {
	data.tables.stats.forEach(function(stat){
		MEUpdateSingleSave(stat);
	});
}
function MEUpdateSingleSave(stat){
	// stat acronym
	let acro = GetAcronym(stat, true);
	// get cr
	let cr = gid('calcTargetCr').value;
	// get prof bool
	let profBool = gid('saveProf' + acro).checked;
	// get prof modifier
	let profBonus = profBool ? data.tables.crStats[cr]['profBonus'] : 0;
	// get bonuses value
	let bonuses = parseInt(gid('saveBonus' + acro ).value) || 0;
	// stat bonuses
	let mod = parseInt(gid( GetAcronym(stat) + 'Mod' ).value) || 0;
	// get save total field
	let field = gid('calcSave' + acro);
	// but value in field
	field.value = profBonus + bonuses + mod;
	// if we're calculating stats, put the save above
	let f = gid('save' + acro);
	if (gid('calcUseStats').checked) {
		f.disabled = true;
		if (profBool){
			f.value = profBonus + bonuses + mod;
		}
		else {
			f.value = '';
		}
	}
	else {
		f.disabled = false;
	}
}
function MEUpdateAttackBonus () {
	// get cr
	let cr = gid('calcTargetCr').value || 0;
	// get attack stat
	let stat = gid('calcAttackStat');
	// get stat mod
	let mod = parseInt(gid( GetAcronym(stat.value) + 'Mod' ).value) || 0;
	// get prof bonus
	let profBonus = data.tables.crStats[cr]['profBonus'];
	// get attack bonus
	let bonus = parseInt(gid('calcAttackBonus').value) || 0;
	// get field
	let field = gid('calcAttackFinalBonus');
	// put data in field
	field.value = mod + profBonus + bonus;
}
function MEUpdateSaveDc () {
	// get cr
	let cr = gid('calcTargetCr').value || 0;
	// get attack stat
	let stat = gid('calcCastingStat');
	// get stat mod
	let mod = parseInt(gid( GetAcronym(stat.value) + 'Mod' ).value) || 0;
	// get prof bonus
	let profBonus = data.tables.crStats[cr]['profBonus'];
	// get attack bonus
	let bonus = parseInt(gid('calcSaveDcBonus').value) || 0;
	// get field
	let field = gid('calcFinalSaveDc');
	// put data in field
	field.value = mod + profBonus + bonus + 8;
}
function MEUpdateAverageDamage () {
	let damage1 = CalculateDiceAverage(gid('calcRoundOneDamage').value);
	let damage2 = CalculateDiceAverage(gid('calcRoundTwoDamage').value);
	let damage3 = CalculateDiceAverage(gid('calcRoundThreeDamage').value);
	let average = Math.round((damage1 + damage2 + damage3) / 3);
	gid('calcAvgDamage').value = average;
}
function MEUpdateDefenses() {
	let cr = gid('calcTargetCr').value;
	let resMul = 1;
	let immMul = 1;
	// get multipliers from CR
	if (cr < 5) {
		resMul = 2;
		immMul = 2;
	}
	else if (cr < 11) {
		resMul = 1.5;
		immMul = 2;
	}
	else if (cr < 17){
		resMul = 1.25;
		immMul = 1.5;
	}
	else {
		resMul = 1;
		immMul = 1.25;
	}
	// should we use the multipliers?
	let resistances = document.querySelectorAll('[id*="damageResistance"]:checked').length;
	let immunities = document.querySelectorAll('[id*="damageImmunity"]:checked').length;
	resMul = resistances > 2 ? resMul : 1;
	immMul = immunities > 2 ? immMul : 1;
	// put multipliers in UI
	gid('calcEHMR').value = resMul;
	gid('calcEHMI').value = immMul;
}
function MECalculateEHP () {
	// figure hitdice
	let dice = gid('calcNumHitDice').value || gid('calcNumHitDice').placeholder;
	let size = gid('monsterSize').value;
	let dieType = 'd' + data.tables.monsterSize[size];
	let hitdice = dice + dieType + "+" + (gid('conMod').value * dice || 0 * dice);
	let hdhp = CalculateDiceAverage(hitdice)
	gid('calcHitDiceTotal').value = hitdice;
	gid('calcSizeHp').value = hdhp;
	// get multipliers
	let resMul = gid('calcEHMR').value;
	let immMul = gid('calcEHMI').value;
	let featMul = gid('calcEHMF').value || 1;
	// calculate total
	let hp = 0;
	let useCalc = gid('calcUseSizeHp').checked;
	if (useCalc){
		hp = hdhp;
	}
	else {
		let declaredHp = gid('calcDeclaredHp').value || 0;
		hp = declaredHp;
	}
	gid('calcFinalHp').value = hp;
	let ehp = hp * resMul * immMul * featMul;
	gid('calcEffectiveHp').value = ehp;
	// place HP if we're calculating stats
	let field = gid('monsterHp');
	if (gid('calcUseStats').checked) {
		field.value = hp;
		field.disabled = true;
	}
	else {
		field.disabled = false;
	}
}
function MECalculateEAC () {
	// get base AC
	let baseAc = parseInt(gid('monsterAc').value) || 12;
	// get ac bonus
	let bonusAc = parseInt(gid('calcEffectiveAcBonus').value) || 0;
	// get field
	let eac = gid('calcEffectiveAc');
	// put ac in UI
	let totalAc = baseAc + bonusAc;
	eac.value = totalAc;
}
function MECalculateCR () {
	// get target CR as a base
	//let cr = gid('calcTargetCr').value || 0;
	// do fancy math shit. Reference DMG pg274

	// lookup monster HP for base defensive cr
	let dcr = LookupHp (gid('calcEffectiveHp').value);
	// now modify the defensive cr
	let eac = parseInt (gid('calcEffectiveAc').value);
	let a = DecimalToFraction(dcr);
	//console.log(a)
	let suggestedAc = data.tables.crStats[a]['armorClass'];
	let acmod = Math.round((eac - suggestedAc) / 2);
	dcr += acmod;

	// now look up the base offensive cr

	// lookup the damage per round
	let dpr = gid('calcAvgDamage').value;
	dpr = parseInt(dpr);
	let dprcr = LookupDpr(dpr);
	a = DecimalToFraction(dprcr);

	// Figure out attack bonus cr mod
	let ab = gid('calcAttackFinalBonus').value;
	let suggestedAb = data.tables.crStats[a]['attackBonus'];
	let abmod = Math.round((ab - suggestedAb) / 2);

	// Figure out dc cr mod
	let dc = gid('calcFinalSaveDc').value;
	let suggestedDc = data.tables.crStats[a]['saveDc'];
	let dcmod = Math.round((dc - suggestedDc) / 2);


	// decide whether to use AB or DC
	let useAb = document.querySelector('input[name="calcOffenseType"]').checked
	let ocr = useAb ? dprcr + abmod : dprcr + dcmod;


	let cr = RoundCr([dcr, ocr]);
	//console.log('dcr', dcr, 'dprcr', dprcr, 'abmod', abmod, 'dcmod', dcmod, 'ocr', ocr, 'cr', cr)

	// Put it in the UI
	// get field
	let field = gid('calcCalcCr');
	// put value in field
	field.value = cr;
	// get toggle
	let useCalc = gid('calcUseStats').checked;
	// set CR
	let finalCrField = gid('monsterCr');
	if (useCalc) {
		finalCrField.value = cr;
		finalCrField.disabled = true;
	}
	else {
		finalCrField.disabled = false;
	}
}
/*

#####    ##   #####   ##   
#    #  #  #    #    #  #  
#    # #    #   #   #    # 
#    # ######   #   ###### 
#    # #    #   #   #    # 
#####  #    #   #   #    # 

*/
function SetEditorClean() {
	//data.isDirty = false;
	// Update UI
}
function SetEditorDirty() {
	//data.isDirty = true;
	// Update UI
}
function LoadDefaultMonsters() {
	let dataUrl = './monsters.json'
	let request = new XMLHttpRequest();
	request.open('GET', dataUrl);
	request.responseType = 'json';
	request.send();
	request.onload = function () {
		data.monsters = request.response;
		//console.log(data.monsters);
		MTInitTable();
	}
}
function LoadFromLS () {

}
function SaveToLS () {

}
function SaveState () {
	// make json
	let text = JSON.stringify(state, null, 2);
	// save to local storage
	localStorage['state'] = text;
}
function LoadState () {
	// load json
	let text = localStorage['state'];
	if (text){	
		// parse state
		state = JSON.parse(text);
		// apply state
		SetPage(state.page);
	}
}

/* 

#    # ##### # #      # ##### # ######  ####  
#    #   #   # #      #   #   # #      #      
#    #   #   # #      #   #   # #####   ####  
#    #   #   # #      #   #   # #           # 
#    #   #   # #      #   #   # #      #    # 
 ####    #   # ###### #   #   # ######  ####

*/
function gid (id) {
	return document.getElementById(id);
}
function toArray(nodelist){
	return Array.from(nodelist);
}
function GetRowFromName (monsterName){
	return document.querySelector('tr[data-monster="' + monsterName + '"');
}
function GetAcronym(stat, upper){
	let acro = stat.slice(0, 3)
	if (!upper){
		acro = acro.toLowerCase();
	}
	return acro;
}
function CalculateDiceAverage(string) {
	let total = 0;
	let delimiters = ['\\s', '\\+', '\\-'];
	let sep = /(?=\s)|(?=\+)|(?=\-)/;
	let array = string.split(sep);
	let a2 = [];
	for( let i = 0; i < array.length; i++) {
		let element = array[i];
		let containsNumber = /\d/.test(element);
		if(element && element !== ' ' && containsNumber){
			a2.push(element.trim());
		}
		else if (element === '+' || element === '-') {
			array[i + 1] = element.trim() + array[i + 1].trim();
		}
	};
	array = a2;
	array.forEach( function(element, index) {
		// add or subtract
		let sign = element.includes('-') ? -1 : 1;
		// remove sign
		element = element.replace('-', '').replace('+', '');

		if (element.includes('d')) {
			let split = element.split('d');
			let dice = parseInt(split[0]);
			let size = parseInt(split[1]);
			let avg = (size / 2) + 0.5;
			total += Math.round(dice * avg) * sign;
		}
		else {
			total += Math.round(parseInt(element) || 0) * sign;
		}
	});
	//console.log(array)
	return total;
}
function GetKeyByValue (object, field, value) {
	return Object.keys(object).find(key => object[field][key] === value);
}
function LookupHp (value) {
	//let cr = 0;
	value = parseInt(value);
	let array = data.tables.crStats;
	let keys = Object.keys(array);
	for (let i = 0; i < keys.length; i++) {
		let key = keys[i];
		let top = array[key]['hitPointMax'];
		let bottom = array[key]['hitPointMin'];
		//console.log(bottom, value, top)
		if ( value >= bottom && value <= top ) {
			cr = FractionToDecimal(key);
			console.log('lookup hp cr', cr);
			return cr;
		}
	};
	return 0;
}
function LookupDpr (value) {
	//let cr = 0;
	value = parseInt(value);
	let array = data.tables.crStats;
	let keys = Object.keys(array);
	for (let i = 0; i < keys.length; i++) {
		let key = keys[i];
		let top = array[key]['damagePerRoundMax'];
		let bottom = array[key]['damagePerRoundMin'];
		//console.log(bottom, value, top)
		if ( value >= bottom && value <= top ) {
			cr = FractionToDecimal(key);
			//console.log('lookup dpr cr',cr);
			return cr;
		}
	};
	return 0;
}
function FractionToDecimal (fraction) {
	//console.log('fraction', fraction)
	fraction = fraction.toString();
	let split = fraction.split('/');
	if (split.length > 1) {
		let numerator = parseInt(split[0]);
		let divisor = parseInt(split[1]);
		let decimal = numerator / divisor;
		return decimal;
	}
	else {
		return parseInt(fraction);
	}
}
function DecimalToFraction (decimal) {
	if (decimal <= 0) {
		return "0";
	}
	if (decimal < 1) {
		let divisor = 1 / decimal;
		let fraction = "1/" + divisor;
		//console.log(decimal, divisor, fraction)
		return fraction;
	}
	else {
		return decimal.toString();
	}
}
function RoundCr (array) {
	if (Array.isArray(array)){
		let total = 0;
		array.forEach(function(number){
			total += FractionToDecimal(number);
		});
		let avg = total / array.length;
		let cr = 0;
		if (avg <= 0) {
			cr = 0;
		}
		else if (avg < 1) {
			avg *= 8;
			avg = Math.round(avg);
			avg /= 8;
			cr = DecimalToFraction(avg);
		}
		else {
			cr = Math.round(avg);
		}
		return cr;
	}
	throw "don't give this a non-array!";
	return;
}