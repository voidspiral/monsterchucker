document.addEventListener("DOMContentLoaded", function() {
	// load data
	LoadDefaultMonsters();
	LoadFromLS();
	// setup page
	SetupNav();
	MTGetColumns();
	SetupFields();
	UpdateCalculatedFields();
	LoadState();
});